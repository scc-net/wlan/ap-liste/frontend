$(document).ready(function () {
    // Disable enter button for search field
    $(document).keypress(function (event) {
        if (event.which === 13) {
            event.preventDefault();
        }
    });

    // get timestamp
    function two_digits (num){
        return ("0" + num).slice(-2)
    }
    $.ajax({
        url: api_server + '/timestamp',
        type: 'GET',
        dataType: 'JSON',
        success: function (response) {
            let timestamp = new Date(response * 1000)
            $('#timestamp-button span').text(timestamp.toLocaleString())
        }
    });

    // get version
    $.ajax({
        url: api_server + '/version',
        type: 'GET',
        dataType: 'JSON',
        success: function (response) {
            let link = 'https://gitlab.net.scc.kit.edu/scc-net-wlan/ap-liste/api/tree/'+response
            $('#gitlab-backend-link-icon').attr('href', link)
            $('#gitlab-backend-link').attr('href', link)
            $('#gitlab-backend-link').text(response)
        }
    });

    // Help overlay & button
    $('#help-button').click(function () {
        $('#help-overlay').css('display', 'block')
    });

    $('#close-overlay').click(function () {
        $('#help-overlay').css('display', 'none')
    });
    $('#help-overlay').click(function (e) {
        if (e.target !== this) {
            return;
        }
        $('#help-overlay').css('display', 'none')
    });
});