let api_server = 'https://ap-liste-api.tmn.scc.kit.edu'
function set_row_error (row, key, value) {
    $(row).addClass('table-danger')
    $(row).attr('title', `<b>${key}:</b> ${value}<br>`)
    $(row).tooltip({html: true,})
}
$(document).ready(function () {
    let search_field = $('#search_field')
    search_field.val('') // Resets search-field on page-reload
    $('[data-toggle="tooltip"]').tooltip() // Activates bootstrap tooltips.

    let table = $('#ap_list').DataTable({
        processing: true,
        ajax: {
            url: api_server + '/aplist',
            dataSrc: ''
        },
        columns: [
            {data: 'name'},
            {data: 'nd_module_type'},
            {
                data: 'nd_device',
                className: 'text-center',
                render: function(data, type, row, meta){
                    if (type === 'filter'){
                        return ''
                    } else if (type === 'display'){
                        return (data === true) ? '<i class="text-success fas fa-check"></i>' : '<i class="text-danger fas fa-times"></i>'
                    } else {
                        return data
                    }
                }
            },
            {data: 'nd_connection_switch'},
            {data: 'nd_connection_port'},
            {data: 'nd_connection_type'},
            {data: 'nd_wall_port'},
            {
                data: null,
                render: function(data, type, row, meta){
                    let snmp_connection_speed, nd_connection_speed
                    if (data['nd_connection_speed'] === null){
                        nd_connection_speed = "-"
                    } else {
                        nd_connection_speed = data['nd_connection_speed']
                    }
                    if (data['snmp_connection_speed'] === null) {
                        snmp_connection_speed = '-'
                    } else {
                        snmp_connection_speed = data['snmp_connection_speed']
                    }
                    if ('SNMP' in data['errors']) {
                        snmp_connection_speed = '<i class="fas fa-exclamation-circle text-warning"></i>'
                    }

                    return `${nd_connection_speed} / ${snmp_connection_speed}`
                }
            },
            {
                data: null,
                render: function(data, type, row, meta) {
                    if ('SNMP' in data['errors']){
                        return '<i class="fas fa-exclamation-circle text-warning"></i>'
                    } else {
                        if (data['power_consumption'] === '?') {
                            return '<i class="far fa-question-circle text-secondary"></i>'
                        } else {
                            return data['power_consumption']
                        }
                    }
                }
            },
            {
                data: 'icinga_ping',
                className: 'text-center',
                render: function (data, type, row, meta) {
                    if (type === 'display'){
                        if (data === 'up'){
                            return '<i class="text-success fas fa-check"></i>'
                        } else if (data === 'down'){
                            return '<i class="text-danger fas fa-times">'
                        } else if (data === 401){
                            return '<i class="text-warning fas fa-exclamation-circle"></i>'
                        } else if (data === '-'){
                            return data
                        } else {
                            return '<i class="text-danger fas fa-exclamation-triangle">' + data + '</i>'
                        }
                    } else {
                        return data
                    }
                }
            },
            {data: 'arubaapi_mac'},
            {data: 'arubaapi_serialnumber'},
        ],
        createdRow: function( row, data ) {
            if ('ND-Connection' in data['errors']) {
                set_row_error(row, 'ND-Connection', data['errors']['ND-Connection'])
            } else if ('ND-Device' in data['errors']) {
                set_row_error(row, 'ND-Device', data['errors']['ND-Device'])
            } else if ('SNMP' in data['errors']) {
                let speed_icon = $(row).find('td:eq(7) > i')
                let power_icon = $(row).find('td:eq(8) > i')
                speed_icon.attr('title', data['errors']['SNMP'])
                power_icon.attr('title', data['errors']['SNMP'])
                speed_icon.tooltip({html: true,})
                power_icon.tooltip({html: true,})
            }

            if (data['snmp_connection_speed'] !== null && data['nd_connection_speed'] !== null) {
                if (data['snmp_connection_speed'] !== data['nd_connection_speed']) {
                    $(row).find('td:eq(7)').addClass('table-warning')
                }
            }
        },
        pagingType: 'full_numbers',
        lengthMenu: [[5, 10, 20, 50, 100, -1], [5, 10, 20, 50, 100, 'All']],
        pageLength: 20,
        dom: "<'row'<'col-12 col-md-5'l><'col-12 col-md-7'p>>" + "<'row'<'col-12'tr>>" + "<'row'<'col-12 col-md-5'i><'col-12 col-md-7'p>>",
        language: {
            "sEmptyTable":      "Keine Daten in der Tabelle vorhanden",
            "sInfo":            "_START_ bis _END_ von _TOTAL_ Einträgen",
            "sInfoEmpty":       "Keine Daten vorhanden",
            "sInfoFiltered":    "(gefiltert von _MAX_ Einträgen)",
            "sInfoPostFix":     "",
            "sInfoThousands":   ".",
            "sLengthMenu":      "_MENU_ Einträge anzeigen",
            "sLoadingRecords":  "Wird geladen ..",
            "sProcessing":       '<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Bitte warten ..</span>',
            "sSearch":          "Suchen",
            "sZeroRecords":     "Keine Einträge vorhanden",
            "oPaginate": {
                "sFirst":       "Erste",
                "sPrevious":    "Zurück",
                "sNext":        "Nächste",
                "sLast":        "Letzte"
            },
            "oAria": {
                "sSortAscending":  ": aktivieren, um Spalte aufsteigend zu sortieren",
                "sSortDescending": ": aktivieren, um Spalte absteigend zu sortieren"
            },
            "select": {
                "rows": {
                    "_": "%d Zeilen ausgewählt",
                    "0": "",
                    "1": "1 Zeile ausgewählt"
                }
            },
            "buttons": {
                "print":    "Drucken",
                "colvis":   "Spalten",
                "copy":     "Kopieren",
                "copyTitle":    "In Zwischenablage kopieren",
                "copyKeys": "Taste <i>ctrl</i> oder <i>\u2318</i> + <i>C</i> um Tabelle<br>in Zwischenspeicher zu kopieren.<br><br>Um abzubrechen die Nachricht anklicken oder Escape drücken.",
                "copySuccess": {
                    "_": "%d Zeilen kopiert",
                    "1": "1 Zeile kopiert"
                },
                "pageLength": {
                    "-1": "Zeige alle Zeilen",
                    "_":  "Zeige %d Zeilen"
                }
            }
        }
    })

    // Function for filter column with regex. gets called by vendor & site filter-buttons.
    function update_filter(group, col) {
        let items = $(group).find('button.active').map(function () {
            return $(this).attr('data-filter')
        }).toArray().join('|')
        table.column(col).search(items, true, false).draw()
    }

    // get counters
    $.ajax({
        url: api_server + '/counters',
        type: 'GET',
        dataType: 'JSON',
        success: function(response){
            // Set filter badges
            $('#cs-badge').text(response['site_counters']['cs'])
            $('#cn-badge').text(response['site_counters']['cn'])
            $('#misc-site-badge').text(response['site_counters']['misc'])

            $('#aruba-badge').text(response['vendor_counters']['aruba'])
            $('#misc-vendor-badge').text(response['vendor_counters']['misc'])

            $('#up-badge').text(response['ping_counters']['up'])
            $('#down-badge').text(response['ping_counters']['down'])
            $('#none-badge').text(response['ping_counters']['-'])

            let error_counter = response['error_counter']
            $('#error-badge').text(error_counter)
            if (error_counter === 0) {
                $('#error-filter').removeClass('btn-outline-danger').addClass('btn-outline-success')
            }
            $('#speed-badge').text(response['speed_counter'])
        }
    })

    // Initalizes filter-buttons in filter-toolbar. Each button adds or removes string to/from Filter-"Array".
    $('#site-filters').click(function (event) {
        $(event.target).button('toggle')
        update_filter(this, 0)
    })
    $('#vendor-filters').click(function (event) {
        $(event.target).button('toggle')
        update_filter(this, 1)
    })
    $('#ping-filters').click(function (event) {
        $(event.target).button('toggle')
        update_filter(this, 9)
    })
    // Clears filter-strings. Deactivates all buttons of this toolbar.
    $('#reset-filters').click(function () {
        table.columns([0, 1, 9]).search('').draw()
        $('#filter-toolbar').find('button.active').removeClass('active')
        $.fn.DataTable.ext.search = []
        table.draw()
    })

    // External search-field. Filters in all cols.
    search_field.keyup(function () {
        table.search($(this).val(), true, false).draw()
    })
    // Search reset button. Clears search array.
    $('#search_reset').click(function () {
        table.search('').draw()
    })
    
    // Error filter button
    function error_filter(settings, data, dataIndex) {
        return $(table.row(dataIndex).node()).hasClass('table-danger')
    }
    // filters table for all rows (<tr>) which have the 'table-danger' class with custom filter function
    $('#error-filter').click( function () {
        $(this).button('toggle')
        if ($(this).hasClass('active')) {
            $.fn.DataTable.ext.search.push(error_filter)
        }
        else {
            $.fn.DataTable.ext.search.splice($.inArray(error_filter, $.fn.dataTable.ext.search), 1 )
        }
        table.draw()
    })

    // Error filter button
    function speed_filter(settings, data, dataIndex) {
        return $(table.row(dataIndex).node()).find('td:eq(7)').hasClass('table-warning')
    }
    // filters table for all rows (<tr>) which have the 'table-danger' class with custom filter function
    $('#speed-filter').click( function () {
        $(this).button('toggle')
        if ($(this).hasClass('active')) {
            $.fn.DataTable.ext.search.push(speed_filter)
        }
        else {
            $.fn.DataTable.ext.search.splice($.inArray(speed_filter, $.fn.dataTable.ext.search), 1 )
        }
        table.draw()
    })


})