$(document).ready(function () {
    function resizeEvent() {
        let filterToolbar = $('#filter-toolbar');
        if ($(document).width() < 1200) {
            filterToolbar.addClass('collapse').removeClass('show');
        } else {
            if ( filterToolbar.hasClass('collapse') ) {
                filterToolbar.removeClass('collapse show');
            }
        }
    }

    let resizeTimer;
    $(window).resize(function () {
        clearTimeout(resizeTimer);
        resizeTimer = setTimeout(resizeEvent, 1000);
    });
    resizeEvent();
});