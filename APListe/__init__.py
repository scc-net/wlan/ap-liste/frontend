import os

from flask import Flask


def build_app():
    application = Flask(__name__, instance_relative_config=True)
    application.config["SECRET_KEY"] = os.getenv("FLASK_SECRET_KEY", "dev")
    return application


app = build_app()

import APListe.views
