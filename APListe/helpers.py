import git


def get_version():
    repo = git.Repo(search_parent_directories=True)
    try:
        version = [tag for tag in repo.tags if tag.commit == repo.head.commit][0]
    except IndexError:
        version = repo.head.object.hexsha
    return version
