from flask import render_template

from APListe import app

from . import helpers


@app.route("/", methods=["GET"])
def home():
    return render_template("home.html", version=helpers.get_version())
