# AP-Liste Frontend

Rewrite of the old wifi-ap-list-generator (php) in python.
The website is generated and served by flask. This frontend needs an [api-server](https://gitlab.net.scc.kit.edu/scc-net-wlan/ap-liste/api) to get data.

## Runing
### Webserver
Config is loaded via environment Variables. Variables that should be set in **production**:
 * `FLASK_ENV` set to `production`, for dev set it to `development`
 * `FLASK_DEBUG` set to `1` or `0` 
 * `FLASK_SECRET_KEY` set to random string, generate it with `python -c 'import os; print(os.urandom(16))'`

Run app with `python3 run.py` or via uwsgi or similar.

## Dependencies

### Production
 * python >= 3.7
 * nodejs / npm

For python-package dependencies see `pyproject` and `poetry.lock`.
For deployment is an synced `requirements.txt`
If you have installed poetry, install the Dependencies with `poetry install`

Run `npm install` in `APListe/static` to install web-dependecies.